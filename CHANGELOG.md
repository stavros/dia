# Changelog


## Unreleased

### Fixes

* Requirements? Where we're going, we don't need requirements! [Stavros Korokithakis]


## v0.1.8 (2022-02-20)

### Features

* Add the "show week" command. [Stavros Korokithakis]

### Fixes

* Fix calling "standup" with an empty log. [Stavros Korokithakis]


## v0.1.7 (2022-02-16)

### Features

* Add the "standup" command. [Stavros Korokithakis]

* Add the "show people|tags|projects" commands. [Stavros Korokithakis]

* Allow passing a regex to the "search" command. [Stavros Korokithakis]

* Add the "edit" command. [Stavros Korokithakis]

### Fixes

* Reduce whitespace between elements. [Stavros Korokithakis]


## v0.1.6 (2022-02-11)

### Features

* Refactor "show" and add "show previous" command. [Stavros Korokithakis]

### Fixes

* Don't use pager on search. [Stavros Korokithakis]

* Don't show empty days in the "search" command. [Stavros Korokithakis]


## v0.1.5 (2022-02-11)

### Features

* Wrap long task text. [Stavros Korokithakis]

* Add "search" command. [Stavros Korokithakis]

### Fixes

* Fix diary config name. [Stavros Korokithakis]

* Don't crash if there are no tasks. [Stavros Korokithakis]


## v0.1.4 (2022-02-10)

### Features

* Add "show" command. [Stavros Korokithakis]

* Add config file. [Stavros Korokithakis]


## v0.1.3 (2022-02-10)

### Features

* Add "today" command and colors. [Stavros Korokithakis]

* Add the name of the day. [Stavros Korokithakis]


## v0.1.2 (2022-02-09)

### Fixes

* Fix the help text for the "log" command. [Stavros Korokithakis]


## v0.1.1 (2022-02-09)

### Fixes

* Don't die if the diary file doesn't exist. [Stavros Korokithakis]


## v0.1.0 (2022-02-09)

### Fixes

* Fix program symlink. [Stavros Korokithakis]

* Fix program symlink. [Stavros Korokithakis]


