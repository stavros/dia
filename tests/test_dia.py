import re

from click.testing import CliRunner

from dia.cli import cli
from dia.cli import DEFAULT_FILENAME
from dia.data_types import Diary


def test_search():
    runner = CliRunner()
    d = """
3000-01-01
* Hi.
2021-01-01
* Hello #task.
* Another task, unrelated.
* More #task.
* Yet some more stuff.
"""
    with runner.isolated_filesystem():
        with open(DEFAULT_FILENAME, "w") as diary_file:
            diary_file.write(d)
        result = runner.invoke(cli, args=["search", "#task"])
        assert result.exit_code == 0
        assert "#task" in result.stdout_bytes.decode()

        diary = Diary.from_text(d)
        # Assert that searching #task in the above diary returns two tasks.
        assert len(diary.filter_text(re.compile(r"\#task", re.IGNORECASE)).tasks) == 2


def test_previous():
    runner = CliRunner()
    with runner.isolated_filesystem():
        with open(DEFAULT_FILENAME, "w") as diary_file:
            diary_file.write(
                """
3000-01-01
* Hi.
2021-01-01
* Hello.
"""
            )
        result = runner.invoke(cli, args=["show", "previous"])
        assert result.exit_code == 0
        assert "Hello" in result.stdout_bytes.decode()

        result = runner.invoke(cli, args=["log", "Test task."])
        assert result.exit_code == 0

        result = runner.invoke(cli, args=["show", "previous"])
        assert result.exit_code == 0
        assert "Hello" in result.stdout_bytes.decode()


def test_diary():
    runner = CliRunner()
    with runner.isolated_filesystem():
        result = runner.invoke(cli, args=["show", "week"])
        assert result.exit_code == 0
        assert "No tasks" in result.stdout_bytes.decode()

        result = runner.invoke(cli, args=["log", "Test task."])
        assert result.exit_code == 0

        result = runner.invoke(cli, args=["search", "test"])
        assert result.exit_code == 0
        assert "Test task" in result.stdout_bytes.decode()

        result = runner.invoke(cli, args=["show", "week"])
        assert result.exit_code == 0
        assert "Test task" in result.stdout_bytes.decode()

        result = runner.invoke(cli, args=["show", "today"])
        assert result.exit_code == 0
        assert "Test task" in result.stdout_bytes.decode()

        result = runner.invoke(cli, args=["show", "previous"])
        assert result.exit_code == 1
        assert "No previous tasks" in result.stdout_bytes.decode()

        result = runner.invoke(cli, args=["show"])
        assert result.exit_code == 0
        assert "Test task" in result.stdout_bytes.decode()

        result = runner.invoke(cli, args=["search", "test"])
        assert result.exit_code == 0
        assert "Test task" in result.stdout_bytes.decode()


def test_no_diary():
    runner = CliRunner()
    with runner.isolated_filesystem():
        result = runner.invoke(cli, args=["search", "test"])
        assert result.exit_code == 0
        assert "No tasks yet" in result.stdout_bytes.decode()

        result = runner.invoke(cli, args=["show", "today"])
        assert result.exit_code == 1
        assert "No tasks yet" in result.stdout_bytes.decode()

        result = runner.invoke(cli, args=["show", "previous"])
        assert result.exit_code == 1
        assert "No previous tasks" in result.stdout_bytes.decode()

        result = runner.invoke(cli, args=["show"])
        assert result.exit_code == 0
        assert "No tasks yet" in result.stdout_bytes.decode()


def test_standup():
    runner = CliRunner()
    with runner.isolated_filesystem():
        result = runner.invoke(cli, args=["standup"])
        assert result.exit_code == 0
        assert "None" in result.stdout_bytes.decode()

        result = runner.invoke(cli, args=["log", "Test task."])
        assert result.exit_code == 0

        result = runner.invoke(cli, args=["standup"])
        assert result.exit_code == 0
        assert "Nothing relevant" in result.stdout_bytes.decode()
